# CV Sharing API #

* stores permissions to individual CV data entries from CV Data service
* retrieves permission restricted version of CV
* allows sharing of permission restricted CVs
* allows tracking you CV views

Project created on Axel Springer Hackaton