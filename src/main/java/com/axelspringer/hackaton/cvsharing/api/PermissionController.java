package com.axelspringer.hackaton.cvsharing.api;

import com.axelspringer.hackaton.cvsharing.models.Permission;
import com.axelspringer.hackaton.cvsharing.repository.PermissionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/permissions")
public class PermissionController {

    @Autowired
    private PermissionRepository permissionRepository;

    @RequestMapping(method = RequestMethod.POST)
    public List<Permission> getPermissions(@RequestBody List<Long> dataIds) {
        return permissionRepository.findByDataIdIn(dataIds);
    }

}
