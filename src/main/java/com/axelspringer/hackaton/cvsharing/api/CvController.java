package com.axelspringer.hackaton.cvsharing.api;

import com.axelspringer.hackaton.cvsharing.models.CvData;
import com.axelspringer.hackaton.cvsharing.models.Link;
import com.axelspringer.hackaton.cvsharing.process.CvDataRetriever;
import com.axelspringer.hackaton.cvsharing.repository.LinkRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/cv/{hash}")
public class CvController {

    @Autowired
    private LinkRepository linkRepository;

    @Autowired
    private CvDataRetriever cvDataRetriever;

    @RequestMapping(method = RequestMethod.GET)
    public List<CvData> getCvDataBasedOnLinkHash(@PathVariable("hash") String hashParam) {
        Link link = linkRepository.findOneByUrl(hashParam);
        if (link == null) {

        }
        link.setVisits(link.getVisits() + 1);
        linkRepository.save(link);

        return cvDataRetriever.getPermissionsForUserAndGroup(link.getUserId(), link.getGroupId());
    }

}
