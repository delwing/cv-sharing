package com.axelspringer.hackaton.cvsharing.api;

import com.axelspringer.hackaton.cvsharing.models.CvData;
import com.axelspringer.hackaton.cvsharing.models.Permission;
import com.axelspringer.hackaton.cvsharing.process.CvDataRetriever;
import com.axelspringer.hackaton.cvsharing.repository.CvDataRepository;
import com.axelspringer.hackaton.cvsharing.repository.PermissionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@RestController
@RequestMapping("/permissions/user/{userId}")
public class CvPermissionsController {

    @Autowired
    private CvDataRetriever cvDataRetriever;

    @Autowired
    private CvDataRepository cvDataRepository;

    @Autowired
    private PermissionRepository permissionRepository;

    @RequestMapping(method = RequestMethod.GET)
    public List<CvData> getPermissionsForUserAndGroup(@PathVariable("userId") long userId, @RequestParam("groupId") long groupId) {
        return cvDataRetriever.getPermissionsForUserAndGroup(userId, groupId);
    }

    @RequestMapping(method = RequestMethod.POST)
    public void savePermissionsForGroup(@PathVariable("userId") long userId, @RequestBody Map<Long, Long> permissions) {
        for (CvData cvData : cvDataRepository.findByUserId(userId)) {
            permissionRepository.deleteByDataId(cvData.getId());
        }
        for (Map.Entry<Long, Long> entry : permissions.entrySet()) {
            Long dataId = entry.getKey();
            Long groupId = entry.getValue();
            Permission permission = new Permission();
            permission.setDataId(dataId);
            permission.setGroupId(groupId);
                permissionRepository.save(permission);
        }
    }
}
