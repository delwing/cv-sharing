package com.axelspringer.hackaton.cvsharing.api;

import com.axelspringer.hackaton.cvsharing.models.Link;
import com.axelspringer.hackaton.cvsharing.process.LinkGenerator;
import com.axelspringer.hackaton.cvsharing.repository.LinkRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/user/{userId}/share")
public class ShareGeneratorController {

    @Value("${url}")
    private String baseUrl;

    @Autowired
    private LinkGenerator linkGenerator;

    @Autowired
    private LinkRepository linkRepository;

    @RequestMapping(method = RequestMethod.POST)
    public String createSharingUrl(
            @PathVariable("userId") long userId,
            @RequestParam("groupId") long groupId) {
        return baseUrl + "/cv/" + linkGenerator.generateLink(userId, groupId).getUrl();
    }

    @RequestMapping(value = "/stats", method = RequestMethod.GET)
    public List<Link> getUserLinks(@PathVariable("userId") long userId) {
        return linkRepository.findByUserId(userId);
    }

}
