package com.axelspringer.hackaton.cvsharing.repository;

import com.axelspringer.hackaton.cvsharing.models.Permission;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface PermissionRepository extends CrudRepository<Permission, Long> {

    Iterable<Permission> findByDataIdInAndGroupId(List<Long> dataIds, Long groupId);

    List<Permission> findByDataIdIn(List<Long> dataIds);

    Permission findByDataId(Long dataId);

    void deleteByDataId(Long dataId);

}
