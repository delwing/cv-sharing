package com.axelspringer.hackaton.cvsharing.repository;

import com.axelspringer.hackaton.cvsharing.models.CvData;
import org.springframework.data.repository.CrudRepository;

public interface CvDataRepository extends CrudRepository<CvData, Long> {

    Iterable<CvData> findByUserId(long userId);

}
