package com.axelspringer.hackaton.cvsharing.repository;

import com.axelspringer.hackaton.cvsharing.models.Link;
import org.springframework.data.repository.CrudRepository;

import java.util.List;

public interface LinkRepository extends CrudRepository<Link, Long> {

    Link findOneByUrl(String url);

    Link findOneByUserIdAndGroupId(long userId, long groupId);

    List<Link> findByUserId(long userId);


}
