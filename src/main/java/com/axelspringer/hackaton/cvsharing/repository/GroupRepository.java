package com.axelspringer.hackaton.cvsharing.repository;

import com.axelspringer.hackaton.cvsharing.models.Group;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.rest.core.annotation.RepositoryRestResource;

@RepositoryRestResource(collectionResourceRel = "groups", path = "groups")
public interface GroupRepository extends CrudRepository<Group, Long> {

}
