package com.axelspringer.hackaton.cvsharing.models;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Table(name = "permissions")
public class Permission {

    @Id
    @GeneratedValue
    @Column(name = "permission_id")
    private long id;

    @Column(name = "data_id")
    private long dataId;

    @Column(name = "group_id")
    private long groupId;

//    @OneToOne
//    @JoinColumn(name = "data_id", insertable = false, updatable = false)
//    private CvData cvData;

    public long getId() {
        return id;
    }

//    public CvData getCvData() {
//        return cvData;
//    }
//
//    public void setCvData(CvData cvData) {
//        this.cvData = cvData;
//    }

    public void setId(long id) {
        this.id = id;
    }

    public long getDataId() {
        return dataId;
    }

    public void setDataId(long dataId) {
        this.dataId = dataId;
    }

    public long getGroupId() {
        return groupId;
    }

    public void setGroupId(long groupId) {
        this.groupId = groupId;
    }

    @Override
    public String toString() {
        final StringBuilder sb = new StringBuilder("Permission{");
        sb.append("id=").append(id);
        sb.append(", dataId=").append(dataId);
        sb.append(", groupId=").append(groupId);
        sb.append('}');
        return sb.toString();
    }
}
