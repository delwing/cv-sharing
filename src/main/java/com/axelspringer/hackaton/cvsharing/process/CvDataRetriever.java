package com.axelspringer.hackaton.cvsharing.process;

import com.axelspringer.hackaton.cvsharing.models.CvData;
import com.axelspringer.hackaton.cvsharing.repository.CvDataRepository;
import com.axelspringer.hackaton.cvsharing.repository.PermissionRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import java.util.ArrayList;
import java.util.List;

@Component
public class CvDataRetriever {

    @Autowired
    private CvDataRepository cvDataRepository;

    @Autowired
    private PermissionRepository permissionRepository;

    public List<CvData> getPermissionsForUserAndGroup(@PathVariable("userId") long userId, @RequestParam("groupId") long groupId) {
        Iterable<CvData> cvDataForUser = cvDataRepository.findByUserId(userId);
        List<CvData> cvDatas = new ArrayList<>();
        for (CvData cvData : cvDataForUser) {
            System.out.println(cvData);
            System.out.println(cvData.getPermission());
            if (cvData.getPermission() == null) {
                cvData.setPermission(permissionRepository.findByDataId(cvData.getId()));
            }
            if (cvData.getPermission().getGroupId() <= groupId) {
                cvDatas.add(cvData);
            }
        }
        return cvDatas;
    }
}
