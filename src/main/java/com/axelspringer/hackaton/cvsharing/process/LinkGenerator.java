package com.axelspringer.hackaton.cvsharing.process;

import com.axelspringer.hackaton.cvsharing.models.Link;
import com.axelspringer.hackaton.cvsharing.repository.LinkRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.util.UUID;

@Component
public class LinkGenerator {

    @Autowired
    private LinkRepository linkRepository;

    private final static int MINIMUM_LENGTH = 2;

    public Link generateLink(long userId, long groupId) {

        Link existingLink = linkRepository.findOneByUserIdAndGroupId(userId, groupId);
        if (existingLink != null) {
            return existingLink;
        }

        UUID uuid = UUID.randomUUID();
        String url;
        int mimumumLength = MINIMUM_LENGTH;
        do {
            url = uuid.toString().replaceAll("-", "").substring(0, ++mimumumLength);
        } while (linkRepository.findOneByUrl(url) != null);
        return linkRepository.save(createLink(userId, groupId, url));
    }

    private Link createLink(long userId, long groupId, String url) {
        Link link = new Link();
        link.setGroupId(groupId);
        link.setUserId(userId);
        link.setUrl(url);
        link.setVisits(0);
        return link;
    }

}
