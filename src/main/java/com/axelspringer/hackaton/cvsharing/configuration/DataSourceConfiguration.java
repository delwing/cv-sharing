package com.axelspringer.hackaton.cvsharing.configuration;

import org.h2.jdbcx.JdbcDataSource;
import org.h2.tools.Server;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;

import javax.naming.NamingException;
import javax.sql.DataSource;
import java.sql.SQLException;

@Configuration
@Profile("h2")
public class DataSourceConfiguration {

    private JdbcDataSource dataSource;
    private Server server;

    @Bean
    public DataSource getDataSource() throws NamingException, SQLException {
        dataSource = new JdbcDataSource();
        dataSource.setUrl("jdbc:h2:tcp://localhost/~/test");
        dataSource.setUser("sa");
        dataSource.setPassword("");
        // start the server
        server = Server.createTcpServer("-tcpAllowOthers").start();
        return dataSource;
    }
}

